﻿using System.Reflection;
using LELCS;
using LELCS.Model;
using LELEngine;
using OpenTK;
using OpenTK.Graphics;

namespace TestGame
{
	internal class Program
	{
		#region PrivateMethods

		//TODO: Replace this debug setup with serialized scene loading
		private static void Main(string[] args)
		{
			Game.CreateWindow(800, 600, "LELEngine");

			// Load Default Scene
			Game.MainController.LoadDefaultScene();

			// Setup scene for debug
			GameObject cam = Game.MainController.ActiveScene.CreateGameObject("MainCamera");
			GameObject CameraRig = Game.MainController.ActiveScene.CreateGameObject("CameraRig");
			GameObject lightRig = Game.MainController.ActiveScene.CreateGameObject("LightRig");
			GameObject light = Game.MainController.ActiveScene.CreateGameObject("LDirectional");

			// Setup light
			light.transform.SetParent(lightRig.transform);
			light.transform.localRotation = Quaternion.Identity;
			light.transform.localPosition = new Vector3(0, 0, -200);
			light.transform.scale = Vector3.One * 10;
			light.AddComponent<DirectionalLight>();

			lightRig.transform.position = Vector3.Zero;
			lightRig.transform.rotation = QuaternionHelper.Euler(10, 40, 0);

			// Setup sun sphere
			MeshRenderer m = light.AddComponent<MeshRenderer>();
			m.SetMaterial("Sun.material");
			m.SetMesh("sphere.obj");

			// Setup camera
			CameraRig.AddComponent<CameraController>();
			Camera c = cam.AddComponent<Camera>();
			cam.transform.SetParent(CameraRig.transform);
			CameraRig.transform.position = Vector3.Zero;
			CameraRig.transform.rotation = QuaternionHelper.Euler(0, 0, 0);
			cam.transform.localPosition = new Vector3(0, 0, -5f);
			cam.transform.localRotation = QuaternionHelper.Euler(10, 0, 0);
			c.FoV = 70;
			c.FarClip = 1000f;
			c.NearClip = 0.1f;

			// Setup lighting
			Lighting.Ambient.Color = Color4.LightGreen;
			Lighting.Ambient.Strength = 0.3f;
			Lighting.Directional.Color = Color4.AntiqueWhite;
			Lighting.Directional.Strength = 1f;
			Lighting.Specular.Strength = 1f;
			Lighting.Specular.Shine = 32f;

			foreach (GameObject go in Game.MainController.ActiveScene.SceneGameObjects)
			{
				if (go.Name == "Floor")
				{
					MeshRenderer mr = go.AddComponent<MeshRenderer>();
					mr.SetMaterial("Cube.material");
					mr.SetMesh("quad.obj");
					mr.transform.rotation = QuaternionHelper.Euler(0, 0, 90);
					mr.transform.scale = Vector3.One * 500;
					mr.transform.position = new Vector3(0, -10, 10f);
				}
			}

			// Create AxeGraphics
			GameObject axeGraphics = Game.MainController.ActiveScene.CreateGameObject("AxeGraphics");
			axeGraphics.transform.rotation = Quaternion.Identity;
			axeGraphics.transform.scale = Vector3.One;

			axeGraphics.transform.localPosition = Vector3.Zero;
			axeGraphics.transform.localRotation = QuaternionHelper.Euler(0, 90, 0);

			MeshRenderer axeGraphicsMr = axeGraphics.AddComponent<MeshRenderer>();
			axeGraphicsMr.SetMaterial("Axe.material");
			axeGraphicsMr.SetMesh("axe.obj");
			
			Game.MainController.InitializeECSScope(Assembly.GetExecutingAssembly());
			
			ECSManager manager = Game.MainController.ECSManager;
			ECSEntity ecsEntity = manager.CreateEntity();
			manager.SetComponent(ecsEntity, new FrameRateCounterComponent());

			Game.MainController.Run();
			// Main function is frozen until game window closes
		}

		#endregion
	}
}
