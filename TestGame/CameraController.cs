﻿using LELEngine;
using OpenTK;

// Do NOT call "base" in any overridden functions
internal class CameraController : Behaviour
{
	#region PrivateFields

	private Vector3 angle;

	#endregion

	#region UnityMethods

	public override void Start()
	{
		angle = QuaternionHelper.ToEulerAngles(transform.localRotation);
	}

	public override void Update()
	{
		angle.Y -= Time.deltaTime * 10;
		transform.rotation = Quaternion.Slerp(transform.rotation, QuaternionHelper.Euler(angle.X, angle.Y, 0), Time.fixedDeltaTime * 10);
	}

	#endregion
}
